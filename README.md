Docker image for ISC DHCP server
================================

This Docker image is suitable for running a DHCP server for your docker host
network.  It uses the ISC DHCP server, which is bundled with the latest Ubuntu
LTS distribution.

I copied and modified this image from <https://hub.docker.com/r/networkboot/dhcpd/> and
<https://www.github.com/networkboot/docker-dhcpd>.

How to build
============

 1. Install Docker with the instructions on <https://www.docker.com>.
 2. Run `./build` to create the local docker image `rbcollins/dhcpd`.

How to use
==========

The most common use-case is to provide DHCP service to the host network of
the machine running Docker.  For that you need to create a configuration for
the DHCP server, start the container with the `--net host` docker run
option, and specify the network interface you want to provide DHCP service
on.

 1. Create a `data` folder in the current directory.
 2. Create `data/dhcpd.conf` with a subnet clause for the specified
    network interface.  If you need assistance, you can run
    `docker run -it --rm rbcollins/dhcpd man dhcpd.conf` for a description
    of the configuration file syntax. You can also view the DHCPD documentation at
    <https://kb.isc.org/v1/docs/isc-dhcp-44-manual-pages-dhcpdconf> (linked docs are
    for version 4.4).
 3. Run `docker run -it --rm --init --net host -v "$(pwd)/data":/data rbcollins/dhcpd eth0`.
    `dhcpd` will automatically start and display its logs on the console.
    You can press Ctrl-C to terminate the server.

A simple `run` script is also included which makes it quick to iterate on a
configuration until you're satisfied.  That will suffice to get you started.
Execute it with the interface on which you want Docker to listen, such as:
`./run eth0`.

After you have a config you like, you might want to run the container as a daemon  
 that starts every time you boot your Docker host. I have included the file `daemon`  
to demonstrate that.  In short, it runs:
`docker run --restart unless-stopped --init -d --net host -v "$(pwd)"/data:/data rbcollins/dhcpd eth0`

DHCPv6
------

To make the container a DHCP v6 server you have to pass `DHCPD_PROTOCOL=6` as an enviroment variable.

`docker run -it --rm --init -e DHCPD_PROTOCOL=6 --net host -v "$(pwd)/data":/data rbcollins/dhcpd eth0`


Notes
=====

The entrypoint script in the docker image takes care of running the DHCP
server as the same user that owns the `data` folder.  This ensures that the
permissions on the files inside the `data` folder are kept consistent.  If
the `data` folder is owned by root, dhcpd is run as the normal dhcpd user.

If you forget to run the docker container with the `--net host` option the entrypoint
script will warn you that you have probably forgotten it.

If a `/data` volume is not provided with a `dhcpd.conf` inside it, the
container will exit with an error message.

An additional Dockerfile has been added that allows building an image with
the tag `:ldap` which contains the **isc-dhcp-server-ldap** package in
addition to the normal DHCP server package.  Otherwise it behaves in
exactly the same way as the `:latest` tag.  Utility build and run scripts
are also present that work with this variant.

Acknowledgements
================

This image uses the following software components:

 * Ubuntu Linux distribution from <https://www.ubuntu.com>.
 * ISC DHCP server from <https://www.isc.org/downloads/dhcp/>.
 * Dumb-init from <https://github.com/Yelp/dumb-init>.

As stated above, I copied this from <https://hub.docker.com/r/networkboot/dhcpd/> and
<https://www.github.com/networkboot/docker-dhcpd>.  Credit to Robin Smidsrød <robin@smidsrod.no>
for the excellent work.

Copyright & License
===================

This project is copyright 2022 Brian Collins <rbc@rbcollins.net>.

It is licensed under the Apache 2.0 license.

See the file LICENSE for full legal details.
